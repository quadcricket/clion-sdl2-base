/*
 *  SDL2 setup for CLion.
 *  Source derived from https://lazyfoo.net/tutorials/SDL/index.php
 */
#include <stdio.h>
#include <SDL.h>

// definitions
#define SCREEN_WIDTH    320
#define SCREEN_HEIGHT   240
#define true    1
#define false   0

// create a bool type
typedef unsigned char bool;

int main(int argc, char *argv[])
{
        printf("Hello, SDL2 world!\n");

        printf("Initializing SDL...\n");

        //The window we'll be rendering to
        SDL_Window* window = NULL;

        //The surface contained by the window
        SDL_Surface* screenSurface = NULL;

        //Initialize SDL
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
                printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        }
        else
        {
                //Create window
                window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
                if (window == NULL)
                {
                        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
                }
                else
                {
                        //Get window surface
                        screenSurface = SDL_GetWindowSurface(window);

                        //Fill the surface a color
                        SDL_FillRect(screenSurface, NULL, SDL_MapRGB( screenSurface->format, 0x11, 0x11, 0xFF));

                        //Update the surface
                        SDL_UpdateWindowSurface(window);

                        printf("Success!\n");
                }
        }
        //Main loop flag
        bool quit = false;

        //Event handler
        SDL_Event e;

        //While application is running (main game loop)
        while(!quit)
        {
                //Handle events on queue
                while(SDL_PollEvent(&e) != 0)
                {
                        //User requests quit
                        if(e.type == SDL_QUIT)
                        {
                                quit = true;
                        }
                }
        }
        // Quit
        printf("Quitting.");
        //Destroy window
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 0;
}