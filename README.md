# Clion SDL2 Base by Justin Smith (justin@u4e.us)

This is just a "skeleton" or base project that sets up C and SDL2 with Clion on Windows. 
I ran into some trouble getting everything working, so I wanted to make sure I had an archive of it to help myself and others start new SDL2 projects.

- Clone this project 
- Download "SDL2-devel-2.0.14-mingw.tar.gz" from https://www.libsdl.org/download-2.0.php
- Extract the i686-w64-mingw32 folder for 32-bit projects.
- Extract the x86_64-w64-mingw32 folder for 64-bit projects.
- Copy the header files (*.h) from /include/SDL2 folder into clion-sdl2-base/include/SDL2
- Copy the library files (*.lib) from /lib to clion-sdl2-base/lib
- Copy the SDL2.dll from /bin to clion-sdl2-base/bin
- Open Clion and then open the CMakeLists.txt and main.c
- Select the green hammer on the top-right to build, or from the top menu select Build->Build Project
- Select the green arrow to execute, or from the top menu selct Run->Run 'clion-sdl2-base'